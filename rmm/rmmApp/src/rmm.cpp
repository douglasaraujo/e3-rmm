#include <rmm.h>

/** Constructor for the RMM class
  */
RMM::RMM(const char *portName, const char *addressInfo, bool enableDebug)
  : asynPortDriver(portName, 1,
      asynInt32Mask | asynDrvUserMask | asynFloat64Mask,          // Interfaces that we implement
      asynInt32ArrayMask | asynFloat64ArrayMask,                  // Interfaces that do callbacks
      ASYN_MULTIDEVICE | ASYN_CANBLOCK, 1, /* ASYN_CANBLOCK=1, ASYN_MULTIDEVICE=1, autoConnect=1 */
      0, 0)  /* Default priority and stack size */
{
  static const char *functionName = "RMM::RMM";

  int status = asynSuccess;

  debugEnable_ = enableDebug;
  strcpy(addressPort_, addressInfo);

  /* Connect to server */
  status = connect();

  // Setup all of the available parameters obtained from the hardware 
  status = createParams();

}

asynStatus RMM::createParams()
{
  static const char *functionName = "RMM::createParams";

  #define rmmTemperatureString  "TEMPERATURE"
  createParam(rmmTemperatureString, asynParamFloat64, &rmmTemperature_);

  return asynSuccess;
}

asynStatus RMM::debug(const std::string& method, const std::string& msg, int value)
{
  // Check if debug is turned on
  if (debugEnable_){
    // Print out the debug message
    std::cout << method << ": " << msg << " [" << value << "]" << std::endl;
  }
  return asynSuccess;
}

asynStatus RMM::debug(const std::string& method, const std::string& msg, std::map<int, std::string> value)
{
  std::map<int, std::string>::iterator iter;

  // Check if debug is turned on
  if (debugEnable_){
    std::cout << method << ": " << msg << " [std::map" << std::endl;
    // This is a map of data, so log the entire map
    for (iter = value.begin(); iter != value.end(); ++iter) {
      std::cout << "     " << iter->first << " => " << iter->second << std::endl;
    }
    std::cout << "]" << std::endl;
  }
  return asynSuccess;
}

asynStatus RMM::debug(const std::string& method, const std::string& msg, const std::string& value)
{
  // Check if debug is turned on
  if (debugEnable_){
    // Copy the string
    std::string val = value;
    // Trim the output
    val.erase(val.find_last_not_of("\n\r")+1);
    // Print out the debug message
    std::cout << method << ": " << msg << " [" << val << "]" << std::endl;
  }
  return asynSuccess;
}

/**
 * Connect to the underlying low level Asyn port that is used for comms.
 * This uses the asynOctetSyncIO interface, and also sets the input and output terminators.
 */
asynStatus RMM::connect()
{
  asynStatus status = asynSuccess;
  asynUser *pasynUser = this->asynUser_;
  const char *functionName = "RMM::connect";
  const char *inputEos = "\n";
  const char *outputEos = "\n";

  status = pasynOctetSyncIO->connect(addressPort_, 0, &pasynUser, NULL);
  if (status != asynSuccess){
    asynPrint(pasynUser, ASYN_TRACE_ERROR,
                "%s:%s, status=%d\n",
                driverName, functionName, status);
    return status;
  } 

  status = pasynOctetSyncIO->setInputEos(pasynUser, inputEos, strlen(inputEos));
  status = pasynOctetSyncIO->setOutputEos(pasynUser, outputEos, strlen(outputEos));

  return status;
}

asynStatus RMM::readFloat64(asynUser *pasynUser, epicsFloat64 *value)
{

  int function = pasynUser->reason;
  int status = 0;
  static const char *functionName = "readFloat64";

  if (function == rmmTemperature_) {
    puts("Temperature Function");
  }
  
  //Other functions we call the base class method
  else {
    status = asynPortDriver::readFloat64(pasynUser, value);
  }
  
  return (status==0) ? asynSuccess : asynError;
}


/** Configuration command, called directly or from iocsh */
extern "C" {
  static void RMMConfig(const char *portName, const char *addressInfo, bool enableDebug) {
    new RMM(portName, addressInfo, enableDebug);
  }

  // Code required for iocsh registration of the DCM Heaters controller
  static const iocshArg configArg0 = { "Port name",       iocshArgString};
  static const iocshArg configArg1 = { "Address Info",    iocshArgString};
  static const iocshArg configArg2 = { "Debug",           iocshArgInt   };

  static const iocshArg * const configArgs[] = {&configArg0,
                                                &configArg1,
                                                &configArg2};

  static const iocshFuncDef configFuncDef = {"RMMConfig", 3, configArgs};

  static void configCallFunc(const iocshArgBuf *args) {
    RMMConfig(args[0].sval, args[1].sval, args[3].ival);
  }

  static void RMMRegister(void) {
    iocshRegister(&configFuncDef, configCallFunc);
  }

  epicsExportRegistrar(RMMRegister);
}