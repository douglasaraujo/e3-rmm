// Standard includes
#include <string.h>
#include <iostream>
#include <map> 
#include <algorithm>
#include <iterator>

// EPICS includes
#include <iocsh.h>
#include <epicsExport.h>

// Asyn driver includes
#include <asynPortDriver.h>
#include <asynOctetSyncIO.h>

static const char *driverName = "RMM";

/** Class definition for the DCMHeaters class
  */
class RMM : public asynPortDriver {
public:
  RMM(const char *portName, const char *addressInfo, bool enableDebug);

  /* These are the methods that we override from asynPortDriver */
  virtual asynStatus readInt32(asynUser *pasynUser, epicsInt32 *value);
  virtual asynStatus readFloat64(asynUser *pasynUser, epicsFloat64 *value);
  asynStatus connect();
  asynStatus createParams();
  
  // Debugging routines
  asynStatus debug(const std::string& method, const std::string& msg, int value);
  asynStatus debug(const std::string& method, const std::string& msg, const std::string& value);
  asynStatus debug(const std::string& method, const std::string& msg, std::map<int, std::string> value);

protected:
  int rmmTemperature_; 
  #define FIRST_RMM_PARAM  rmmTemperature_
  #define LAST_RMM_PARAM   rmmTemperature_

private:
  asynUser *asynUser_;
  char addressPort_[20];
  bool debugEnable_ = 0;

};
