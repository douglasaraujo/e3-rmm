# The following lines are required
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS

# Most modules only need to be built for x86_64
ARCH_FILTER += linux-x86_64

APP:=rmmApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

TEMPLATES += $(wildcard $(APPDB)/*.db)
#TEMPLATES += $(wildcard $(APPDB)/*.template)

USR_INCLUDES += -I$(where_am_I)$(APPSRC)

SOURCES += $(wildcard $(APPSRC)/*.cpp)
HEADERS += $(APPSRC)/rmm.h
DBDS    += $(APPSRC)/rmm.dbd
SCRIPTS += $(wildcard iocsh/*.iocsh)

TMPS=$(wildcard $(APPDB)/*.template)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

.PHONY: vlibs
vlibs:
